#ifndef __MENU_WINDOW__HPP
#define __MENU_WINDOW__HPP

#include <QMainWindow>
#include <QPushButton>

#include "displayWindow.hpp"
#include "automaton.hpp"
#include "ruleset.hpp"

class MenuWindow : public QMainWindow
{
Q_OBJECT
public:
  MenuWindow(QWidget *parent);
  ~MenuWindow();

public slots:
  void buttonPush(bool);

private:
  QPushButton *button;
  DisplayWindow *simulation;
  Ruleset *rules;
};

#endif
