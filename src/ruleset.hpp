#ifndef __RULESET_HPP__
#define __RULESET_HPP__

#include <stdint.h>
#include <array>

class Ruleset{
public:
  static const unsigned int MAX_N=9;
  virtual ~Ruleset();
  virtual bool result(uint16_t allNeighbours)const=0;
  virtual bool result(std::array<bool, MAX_N>& allNeighbours)const=0;
};

template<unsigned int SIZE> 
class FixedLengthRuleset: public Ruleset{
protected:
  static const unsigned int N = SIZE,
                            //the number of possible states in this neighbourhood
                            STATE_COUNT = (1<<N),
                            //the number of int16s to represent all states
                            INTS_REQ = (STATE_COUNT >> 4),
                            MASK = 0xFFFF >> (16-SIZE);
  //eg Moore Neighbourhood
  //9 cell neighbourhood == 2^9 possible permutations (aka 1<<9)
  bool states[STATE_COUNT];

  //each bool is a byte but only stores a bit of information
  //later improvement could be to use every bit in each byte
  //char    states[64] //using bitpacking
  //int64_t states[8] //using bitpacking
  bool getStateResult(int index)const;
public:
  FixedLengthRuleset(std::array<uint16_t, INTS_REQ> ruleCode);
  virtual ~FixedLengthRuleset();
  virtual bool result(uint16_t allNeighbours)const=0;
  bool result(std::array<bool, MAX_N>& allNeighbours)const;
};

/*
  - Instances of the FixedLengthRuleset template are defined below 
  - They must each implement how to filter the full neighbood down to the
  neighbours that they require
  - The filtered down neighbourhood (as an integer) is used as the index into
  the states array
*/

//Moore Neighbourhood
class MRuleset: public FixedLengthRuleset<9>{
public:
  virtual bool result(uint16_t allNeighbours)const;
};

//restricted Moore Neighbourhood (assume central cell state)
class RMRuleset: public FixedLengthRuleset<8>{
public:
  virtual bool result(uint16_t allNeighbours)const;
};

//Von-Neumann Neighbourhood
class VNRuleset: public FixedLengthRuleset<5>{
public:
  virtual bool result(uint16_t allNeighbours)const;

  //for this rule set, it is possible to define a ruling in 32 bits 
  VNRuleset(uint16_t ruleCodeRight, uint16_t ruleCodeLeft);
  VNRuleset(uint32_t ruleCode);
};

//restricted Von-Neumann Neighbourhood (assume central cell state)
class RVNRuleset: public FixedLengthRuleset<4>{
public:
  virtual bool result(uint16_t allNeighbours)const;

  /*
    for this restricted ruleset, it is possible to define a ruling with
    a 16 bit integer
  */
  RVNRuleset(uint16_t ruleCode);
};

#endif
