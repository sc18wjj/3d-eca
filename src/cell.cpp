#include <GL/glu.h>
#include <GL/glut.h>

#include "cell.hpp"

float Cell::defaultSize = 1;

Cell::Cell(int x, int y, float size)
  :x(x), y(y), size(size)
{ }

void Cell::model()
{
  glPushMatrix();
    glTranslatef(x,y,0);
    glutSolidCube(size);
  glPopMatrix();
}

void Cell::setDefaultSize(float size){
  if(size <= 0 || size > 1)return;
  defaultSize=size;
}
