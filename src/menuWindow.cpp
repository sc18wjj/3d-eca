#include "menuWindow.hpp"

MenuWindow:: MenuWindow(QWidget *parent)
: QMainWindow(parent),
  button(new QPushButton("Start")),
  simulation(NULL),
  rules(NULL)
{
  setCentralWidget(button);
  connect(button, SIGNAL(clicked(bool)), this, SLOT(buttonPush(bool)));
}
MenuWindow::~MenuWindow()
{
  if(simulation)delete simulation;
  if(rules)delete rules;
  delete button;
}

void MenuWindow::buttonPush(bool b)
{
  if(simulation){
    delete simulation;
    simulation=NULL;
  }
  if(rules){
    delete rules;
    rules=NULL;
  }
  
  rules = new RVNRuleset(20);
  simulation = new DisplayWindow(NULL, Automaton(*rules, 5));
  simulation->show();
  this->hide();
}
