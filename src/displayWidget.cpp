#include <GL/glu.h>
#include <GL/glut.h>

#include "displayWidget.hpp"

DisplayWidget::DisplayWidget(QWidget *parent, Automaton&& atmt)
  :QGLWidget(parent),
  horiAngle(0), vertAngle(0), zoomFactor(10),
  atmt(std::move(atmt))
{
  //Cell::setDefaultSize(0.9);

  setSizePolicy(QSizePolicy::Policy::MinimumExpanding,
                QSizePolicy::Policy::MinimumExpanding);
}

DisplayWidget::~DisplayWidget()
{ }

void DisplayWidget::initializeGL()
{
  //set background colour
  glClearColor(0.1, 0.1, 0.4, 0.0);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  //set perspective view volume
  glFrustum(-1.0, 1.0,
             -1.0, 1.0,
              1.5, 30.0);
}

void DisplayWidget::resizeGL(int w, int h)
{
  glViewport(0,0,w,h);
}

void DisplayWidget::paintGL()
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glEnable(GL_NORMALIZE);
  glEnable(GL_CULL_FACE);
  //glEnable(GL_BLEND); //used for transparency - may be needed later
  glShadeModel(GL_SMOOTH);

  glMatrixMode(GL_MODELVIEW);
  glEnable(GL_DEPTH_TEST);

  glEnable(GL_LIGHTING);
  glEnable(GL_LIGHT0);
  GLfloat lightPosition[] = {1.0, -10.0, 3.0, 1};

  glLoadIdentity();
  gluLookAt(0,1-zoomFactor,0, 0,0,0, 0,0,1);

  glRotatef(vertAngle, 1,0,0);
  glRotatef(horiAngle, 0,0,1);
  glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);
  glScalef(0.4,0.4,0.4);

  atmt.model();

  glFlush();
}

void DisplayWidget::updateTurn(int sliderVal)
{
  horiAngle = sliderVal;
  repaint();
}
void DisplayWidget::updateTilt(int sliderVal)
{
  vertAngle = sliderVal;
  repaint();
}
void DisplayWidget::updateZoom(int sliderVal)
{
  zoomFactor = sliderVal/10.0;
  repaint();
}

void DisplayWidget::takeStep(bool checked)
{
  atmt.timeStep();
  repaint();
}

QSize DisplayWidget::sizeHint() const
{
  return QSize(500,500);
}
QSize DisplayWidget::minimumSizeHint() const
{
  return QSize(200,200);
}
