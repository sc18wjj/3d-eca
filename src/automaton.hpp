#ifndef __AUTOMATON_HPP__
#define __AUTOMATON_HPP__

#include <vector>

#include "grid.hpp"
#include "gridLogic.hpp"
#include "ruleset.hpp"

class Automaton
{
public:
  Automaton(const Ruleset &r, int size);
  Automaton(Automaton &&that);

  void timeStep();
  void model();
  double topLayerDensity();
  double totalDensity();

protected:
  const int gridSize;
  const Ruleset &rules;
  std::vector<Grid> layers;
};

#endif
