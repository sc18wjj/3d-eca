#ifndef __GRID_LOGIC_HPP__
#define __GRID_LOGIC_HPP__

#include "ruleset.hpp"

class GridLogic{
protected:
  const int width, height;
  bool *data;

  GridLogic(int width, int height);
  GridLogic(GridLogic&& that);

public:
  static GridLogic empty(int width, int height);
  static GridLogic full(int width, int height);
  static GridLogic dot(int width, int height);
  virtual ~GridLogic();

  GridLogic applyRuleset(const Ruleset& rules)const;
};

/*
ISSUES
  (1) Edge Conditions
  (2) Efficiency i)speed ii)space 

IDEAS
  -Circular Linked List
  (1) circular links would solve edges senarios with torus shape
  (2) i) linked list are slower for random access but, so far, all access is
      a loop through all elements which is no slower
      ii) slight increase in space for more pointers

  -Bit packing
  could use every bit of each byte which the bool type doesnt
  (1) n\a
  (2) i) unsure what effect low level bitwise operations have on speed, however
      would potentially mean fewer fetches from memory
      ii) space reduced by a factor of 8
*/
#endif
