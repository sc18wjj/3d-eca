#include <QLayout>

#include "displayWindow.hpp"

LimitedSlider::LimitedSlider(Qt::Orientation orient,
                               int lower, int higher, QWidget *parent)
  : QSlider(orient, parent)
{
  setMinimum(lower);
  setValue((lower+higher)/2);
  setMaximum(higher);
}

OptionBox::OptionBox()
: QWidget(NULL),
  layout(new QBoxLayout(QBoxLayout::TopToBottom)),
  tiltSlider(new LimitedSlider(Qt::Horizontal, -90, 90)),
  turnSlider(new LimitedSlider(Qt::Horizontal, -180, 180)),
  zoomSlider(new LimitedSlider(Qt::Horizontal, 0,200)),
  stepButton(new QPushButton("step")),
  autoToggle(new QPushButton("toggle auto-step"))
{
  setupViewOptions();
  layout->addWidget(viewOptions);

  setupControls();
  layout->addWidget(controls);
  
  this->setLayout(layout);
}

void OptionBox::setupViewOptions()
{
  viewOptions = new QGroupBox(QString("View Options"));
  sliderFormLayout = new QFormLayout();

  sliderFormLayout->addRow(QString("Turn"), turnSlider);
  sliderFormLayout->addRow(QString("Tilt"), tiltSlider);
  sliderFormLayout->addRow(QString("Zoom"), zoomSlider);

  connect(tiltSlider, SIGNAL(valueChanged(int)), this, SIGNAL(tiltChanged(int)));
  connect(turnSlider, SIGNAL(valueChanged(int)), this, SIGNAL(turnChanged(int)));
  connect(zoomSlider, SIGNAL(valueChanged(int)), this, SIGNAL(zoomChanged(int)));

  viewOptions->setLayout(sliderFormLayout);
}
void OptionBox::setupControls()
{
  controls = new QGroupBox(QString("Simulation Controls"));
  buttonLayout = new QBoxLayout(QBoxLayout::LeftToRight);

  autoToggle->setCheckable(true);
  buttonLayout->addWidget(autoToggle);
  buttonLayout->addWidget(stepButton);

  connect(stepButton, SIGNAL(clicked(bool)), this, SIGNAL(stepTaken(bool)));
  connect(autoToggle, SIGNAL(clicked(bool)), this, SIGNAL(autoStepToggled(bool)));
  connect(autoToggle, SIGNAL(clicked(bool)), this, SLOT(toggleStepButton(bool)));

  controls->setLayout(buttonLayout);
}

void OptionBox::toggleStepButton(bool checked)
{
  stepButton->setEnabled(! checked);
}

OptionBox::~OptionBox()
{
  delete tiltSlider;
  delete turnSlider;
  delete zoomSlider;
  delete stepButton;
  delete layout;
}

DisplayWindow::DisplayWindow(QWidget *parent, Automaton&& atmt)
: QWidget(parent),
  mainWidget(new DisplayWidget(NULL, std::move(atmt))),
  options(new OptionBox()),
  layout(new QVBoxLayout(NULL)),
  stepTimer(new QTimer(NULL))
{
  layout->addWidget(mainWidget);
  layout->addWidget(options);

  connect(options, SIGNAL(tiltChanged(int)), mainWidget, SLOT(updateTilt(int)));
  connect(options, SIGNAL(turnChanged(int)), mainWidget, SLOT(updateTurn(int)));
  connect(options, SIGNAL(zoomChanged(int)), mainWidget, SLOT(updateZoom(int)));
  connect(options, SIGNAL(stepTaken(bool)), mainWidget, SLOT(takeStep(bool)));

  connect(options, SIGNAL(autoStepToggled(bool)), this, SLOT(toggleTimer(bool)));

  stepTimer->setInterval(200);
  connect(stepTimer, SIGNAL(timeout()), mainWidget, SLOT(takeStep()));

  this->setLayout(layout);
}

void DisplayWindow::toggleTimer(bool shouldTurnOn)
{
  if(shouldTurnOn)
    stepTimer->start();
  else
    stepTimer->stop();
}

DisplayWindow::~DisplayWindow()
{
  delete mainWidget;
  delete options;
  delete layout;
  delete stepTimer;
}
