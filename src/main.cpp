#include <QApplication>
#include <GL/glut.h>

//#include "displayWindow.hpp"
#include "menuWindow.hpp"

int main(int argc, char** argv)
{
  QApplication app(argc, argv);
  glutInit(&argc, argv);

  //make a window
  MenuWindow *window = new MenuWindow(NULL);
  window->resize(512,512);

  //show window
  window->show();

  app.exec();

  //clean up and close
  delete window;
  return 0;
}
