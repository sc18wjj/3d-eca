#ifndef __DISPLAY_WINDOW_HPP__
#define __DISPLAY_WINDOW_HPP__

#include <QGLWidget>
#include <QBoxLayout>
#include <QGroupBox>
#include <QFormLayout>
#include <QSlider>
#include <QPushButton>
#include <QTimer>

#include "displayWidget.hpp"
#include "automaton.hpp"

class LimitedSlider: public QSlider{
public:
  LimitedSlider(Qt::Orientation, int lower, int higher, QWidget *parent=NULL);
};

class OptionBox : public QWidget{
Q_OBJECT
public:
  OptionBox();
  ~OptionBox();

public slots:
  void toggleStepButton(bool);

signals:
  void tiltChanged(int);
  void turnChanged(int);
  void zoomChanged(int);
  void stepTaken(bool);
  void autoStepToggled(bool);

private:
  QBoxLayout *layout;
  QGroupBox *controls, *viewOptions;

  void setupViewOptions();
  QFormLayout *sliderFormLayout;
  LimitedSlider *tiltSlider, *turnSlider, *zoomSlider;

  void setupControls();
  QBoxLayout *buttonLayout;
  QPushButton *stepButton, *autoToggle;
};

class DisplayWindow: public QWidget
{
Q_OBJECT
public:
  DisplayWindow(QWidget *parent, Automaton&& atmt);
  ~DisplayWindow();

public slots:
  void toggleTimer(bool);

private:
  DisplayWidget *mainWidget;
  OptionBox *options;
  QBoxLayout *layout;
  QTimer *stepTimer;
};

#endif
