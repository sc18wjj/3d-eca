#include <iostream>

#include "ruleset.hpp"

Ruleset::~Ruleset(){}

template<unsigned int SIZE>
FixedLengthRuleset<SIZE>::FixedLengthRuleset(std::array<uint16_t, INTS_REQ> ruleCode)
{
  int index=0;
  for(uint16_t &block : ruleCode)
    for(int bit=0; bit<16; bit++)
      states[index++] = (block & (1<<bit)) ? true : false;
}

template<unsigned int SIZE>
FixedLengthRuleset<SIZE>::~FixedLengthRuleset(){}

template<unsigned int SIZE>
bool FixedLengthRuleset<SIZE>::getStateResult(int index)const
{
  if( index & ~MASK ) 
    std::cerr << "(getStateResult) - index contains bits outside of mask"
              << std::endl;
  //bitmask AND ensures that no bits futher left than N are set
  return states[index & MASK];
}

template<unsigned int SIZE>
bool FixedLengthRuleset<SIZE>::result(std::array<bool, MAX_N>& allNeighbours)
const
{
  //convert bool array to bitpacked int
  int index = 0;
  for(uint16_t i=0; i<MAX_N; i++){
    if(allNeighbours[i]) index |= 1<<(MAX_N-(i+1));
  }
  return result(index);
}

//---------------Moore Neighbourhood Ruleset----------------
bool MRuleset::result(uint16_t allNeighbours)const
{
  //full neighbourhood N == MAX_N
  return getStateResult(allNeighbours);
}
//----------------------------------------------------------
//----------Restricted Moore Neighbourhood Ruleset----------
bool RMRuleset::result(uint16_t allNeighbours)const
{
  //###9 8765 4321 --> #### 9876 4321 
  //bit at position 5 not needed
  const int left = allNeighbours & (0xF0 << 1),
           right = allNeighbours & (0x0F);

  //left 'nibble' shifted right by one to ignore bit 5
  return getStateResult((left >> 1) | right);
}
//----------------------------------------------------------
//------------Von Neumann Neighbourhood Ruleset-------------
bool VNRuleset::result(uint16_t allNeighbours)const
{
  //###9 8765 4321 --> #### ###8 6542
  const int top = allNeighbours & (1 << 7),  //just bit 8
         middle = allNeighbours & (7 << 3),  //bits 456
         bottom = allNeighbours & (1 << 1);  //just bit 2
  
  //shifting required to fill the gaps
  return getStateResult( ((top>>1 | middle)>>1 | bottom)>>1);
}

VNRuleset::VNRuleset(uint16_t ruleCodeUpper, uint16_t ruleCodeLower)
: FixedLengthRuleset<N>({ruleCodeLower, ruleCodeUpper})
{}
VNRuleset::VNRuleset(uint32_t ruleCode)
: VNRuleset( uint16_t(ruleCode>>16), uint16_t(ruleCode))
//32 bit rule code split into two 16 bit ruleCodes
{ }
//----------------------------------------------------------
//-------Restricted Von Neumann Neighbourhood Ruleset-------
bool RVNRuleset::result(uint16_t allNeighbours)const
{
  //###9 8765 4321 --> #### #### 8642

  //the commented lines show a more 'verbose' implementation
  //const int bits[] = {allNeighbours & (1<<7), //bit 8
  //                    allNeighbours & (1<<5), //bit 6
  //                    allNeighbours & (1<<3), //bit 4
  //                    allNeighbours & (1<<1)};//bit 2
  int index=0;
  for (int i=0; i<N; i++){
    //index |= bits[i] >> (4-i); //shift the bits down to one byte
    index |= (allNeighbours & (1<<(7-2*i))) >> (4-i);
  }
  return getStateResult(index);
}

RVNRuleset::RVNRuleset(uint16_t ruleCode)
: FixedLengthRuleset<N>({ruleCode})
{}
//----------------------------------------------------------
