#ifndef __CELL_HPP__
#define __CELL_HPP__ 

class Cell{
protected:
  int x,y;

  static float defaultSize;
  float size;

public:
  Cell(int x, int y, float size=defaultSize);

  void model();

  static void setDefaultSize(float size);

};

#endif
