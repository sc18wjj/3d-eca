#include <GL/glu.h>

#include "grid.hpp"

void Grid::loadCells()
{
  for(int i=0; i<width; i++)
    for(int j=0; j<height; j++)
      if(data[i+j*width])
        cells.push_back(Cell(i,j));
}

Grid::Grid(GridLogic&& that)
  :GridLogic(std::move(that)),
  z(0),
  cells()
{
  loadCells();
}

Grid::Grid(const Grid& parent, const Ruleset& rules)
  :GridLogic(parent.applyRuleset(rules)),
  z(parent.z+1),
  cells()
{
  loadCells();
}

void Grid::model()
{
  for(auto &c : cells){
    c.model();
  }
}

int Grid::liveCellCount()
{
  return cells.size();
}
