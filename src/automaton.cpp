#include <GL/glu.h>

#include <iostream>

#include "automaton.hpp"

Automaton::Automaton(const Ruleset &r, int size)
: gridSize(size),
  rules(r),
  layers()
{
  layers.push_back(GridLogic::dot(gridSize, gridSize));
  
}

Automaton::Automaton(Automaton &&that)
: gridSize(that.gridSize),
  rules(that.rules),
  layers(std::move(that.layers))
{
}

void Automaton::timeStep()
{
  layers.push_back(Grid(layers.back(), rules));
  std::cout << topLayerDensity() << '\t' << totalDensity() << std::endl;
}

void Automaton::model()
{
  glTranslatef((1-gridSize)/2, (1-gridSize)/2, layers.size()/2.0);
  for(auto &l: layers){
    l.model();
    glTranslatef(0,0,-1);
  }
}

double Automaton::topLayerDensity()
{
  const int volume = gridSize*gridSize*1,
    liveCellCount = layers.back().liveCellCount();

  return (double)liveCellCount/volume;
}
double Automaton::totalDensity()
{
  const int volume = gridSize*gridSize*layers.size();
  int liveCellCount =0;
  for(auto &l: layers)
    liveCellCount += l.liveCellCount();

  return (double)liveCellCount/volume;
}
