#ifndef __GRID_HPP__
#define __GRID_HPP__

#include <vector> 

#include "gridLogic.hpp"
#include "cell.hpp"

class Grid : public GridLogic{
protected:
  const int z;
  std::vector<Cell> cells;

  void loadCells();

public:
  Grid(GridLogic&& that);
  Grid(const Grid& parent, const Ruleset& rules);

  void model();
  int liveCellCount();
};

#endif
