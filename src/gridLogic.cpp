#include <array>
#include <iostream>

#include "grid.hpp"

GridLogic::GridLogic(int width, int height)
  :width(width), height(height),
  data(new bool[width*height])
{ }
GridLogic::GridLogic(GridLogic&& that)
  :width(that.width),
  height(that.height),
  data(that.data)
{
  that.data = NULL;
}
GridLogic::~GridLogic(){delete[] data;}

GridLogic GridLogic::empty(int width, int height)
{
  GridLogic result = GridLogic(width, height);
  for(int j=0; j<height; j++)
    for(int i=0; i<width; i++){
      result.data[i+j*width]=false;
    }
  return result;
}
GridLogic GridLogic::full(int width, int height)
{
  GridLogic result = GridLogic(width, height);
  for(int j=0; j<height; j++)
    for(int i=0; i<width; i++){
      result.data[i+j*width]=true; 
    }
  return result;
}
GridLogic GridLogic::dot(int width, int height)
{
  GridLogic result = GridLogic(width, height);
  for(int j=0; j<width; j++)
    for(int i=0; i<width; i++){
      result.data[i+j*width]=false; 
    }
  result.data[(width*height/2)]=true;
  return result;
}

GridLogic GridLogic::applyRuleset(const Ruleset& rules)const
{
  //initialise the next grid
  GridLogic child = GridLogic(width, height);
  std::array<bool,9> neighbourhood;
  int yp=width*(height-2),
      yc=width*(height-1),
      yn=0;

  //fill the new grid using the rule set on the current grid
  for (int j=1; j<=height; j++){
    int xp = width-2,
        xc = width-1,
        xn = 0;
    for (int i=1; i<=width; i++){
       neighbourhood = {
                       data[yp+xp], data[yp+xc], data[yp+xn],
                       data[yc+xp], data[yc+xc], data[yc+xn],
                       data[yn+xp], data[yn+xc], data[yn+xn]
                       };
      child.data[yc+xc]=rules.result(neighbourhood);
      xp=xc;
      xc=xn;
      xn++;
    }
    yp=yc;
    yc=yn;
    yn+=width;
  }
  return child;
}
