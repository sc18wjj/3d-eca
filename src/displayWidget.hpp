#ifndef __CUBE_WIDGET_HPP__
#define __CUBE_WIDGET_HPP__

#include <QGLWidget>

#include "automaton.hpp"

class DisplayWidget: public QGLWidget
{
  Q_OBJECT
public:
  DisplayWidget(QWidget *parent, Automaton &&atmt);
  ~DisplayWidget();

  QSize sizeHint() const;
  QSize minimumSizeHint() const;

public slots:
  void updateTurn(int);
  void updateTilt(int);
  void updateZoom(int);
  void takeStep(bool checked=false);

private:
  float horiAngle, vertAngle, zoomFactor;
  Automaton atmt;

protected:
  void initializeGL();
  void resizeGL(int w, int h);
  void paintGL();
};

#endif
