# Recommended build process:
Ensure you have the following packages:
- `build-essential` (including `make` and a c++ compiler)
- `libqt4-opengl-dev`
- `freeglut3-dev`

1. create a build directory (`mkdir build`)
2. change into that directory (`cd build`)
3. run `qmake` with reference to the previous directory where the `.pro` file is (`qmake ..`)
4. change back to main project directory (`cd ..`)
5. run `make` with reference to the build directory where the `Makefile` is (`make -C build`)
6. the application should have been succesfully compiled in the `bin/` directory

# Altering the Simulation
`src/menuWindow.cpp : line 30` - `rules = new RVNRuleset(20);`
Line 30 creates the ruleset using a ruleset object constructor and a rulecode. The options for the ruleset object are:
- `RVNRuleset` Restricted Von-Neumann Neighbourhood (no use of central cell) with neighbourhood size 4
- `VNRuleset` Von-Neumann Neighbourhood with neighbourhood size 5
- `RMRuleset` Restricted Moore Neighbourhood (no use of central cell) with neighbourhood size 8
- `MRuleset` Moore Neighbourhood with neighbourhood size 9

Further details can be found by reviewing `src/ruleset.hpp`


`src/menuWindow.cpp : line 31` - `simulation = new DisplayWindow(NULL, Automaton(*rules, 5));` 
Line 31 creates the simulation using the ruleset defined above and an integer value for the gridsize. Here the default is 5 however for larger simulations try 129 or 257. For some simulations it can be useful if the grid size is one more than a power of two (`2^k + 1` for some k>1)


Alterations to the graphics can be made in `src/displayWidget.cpp` however this may require an understanding of how OpenGL is used to render the models.

# Reviewing Other Branches
The report for this project describes milestones/iterations in the projects development. These milestones can be viewed using `git checkout` to change the relevant tag/branch:
- `CubeGraphics` tag is the first milestone where cubes are modelled in a simple scene
- `FirstWorking3D-ECA` tag is the milestone where the simple cube scene is first integrated with ECA logic
- this `master` branch is the current completed iteration of the project
- the `develop` branch is the current ongoing development iteration, containing some of the extended features outlined in the project report. This includes a window for inputting a ruleset without re-complilation and a command line interface for the application.

Some of these older versions of the application do not enforce the use of the c++11 standard which can cause issues in the build process. If this is a problem you can add the line `QMAKE_CXXFLAGS += -std=c++11` to the `.pro` file for that application.

# Data
The `data` folder contains a simple python program for plotting data produced by running the application. To produce data of your own, run the application and pipe the output to a file : `/bin/ECA_3D > rule126.tsv`. The same can be done with the command line application available at the `develop` branch but beware of some addition output that will cause issues for the python program. The first 4/5 lines of the output file would have to be manually removed to restore the file to a purely tab-seperated value file (.tsv).

There is also some sample data included in the folder and more data available on the `develop` branch where the data for the evaluation of the report was collected and reviewed.
