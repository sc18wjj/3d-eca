TEMPLATE = app
DESTDIR = ../bin
TARGET  = ECA_3D

#enforce c++11 standard
QMAKE_CXXFLAGS += -std=c++11

LIBS += -lglut -lGLU
QT   += widgets opengl gui

HEADERS += src/displayWindow.hpp \
           src/displayWidget.hpp \
           src/cell.hpp \
           src/ruleset.hpp \
           src/grid.hpp \
           src/gridLogic.hpp \
	   src/automaton.hpp \
	   src/menuWindow.hpp

SOURCES += src/main.cpp   \
           src/displayWindow.cpp \
           src/displayWidget.cpp \
           src/cell.cpp \
           src/ruleset.cpp \
           src/grid.cpp \
           src/gridLogic.cpp \
	   src/automaton.cpp \
	   src/menuWindow.cpp
