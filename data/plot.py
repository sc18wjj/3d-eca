import csv
from matplotlib import pyplot as plt
import numpy as np

def readFile(path):
  y1 = []
  y2 = []
  with open(path) as dataFile:
    reader = csv.reader(dataFile, delimiter='\t')
    for row in reader:
      y1.append(float(row[0]))
      y2.append(float(row[1]))
  return y1,y2
  
y1,y2 = readFile("data/sample201.tsv")
N = max(len(y1), len(y2))

Y1 = np.array(y1)
Y2 = np.array(y2)

X = np.arange(N)

plt.plot(X,Y1)
plt.plot(X,Y2)
plt.show()
